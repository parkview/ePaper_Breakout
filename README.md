# 24pin ePaper Breakout Board 
  
A simple ePaper breakout PCB for 24pin displays that can be hooked up to a lot of different controllers.  Test code is via a ESP32-WROOM example.  Connect up the 24pin ePaper cable, with the contacts facing up.
  
For more documentation visit [https://forum.swmakers.org/viewtopic.php?f=9&t=2490](https://forum.swmakers.org/viewtopic.php?f=9&t=2490)
  
## Hardware Features:
---------
#### Version 1.0 Features: 

* jumper J3 to measure the power needed to run the ePaper display
* inductor and MOSFET are spread out, so as to make it easy to hot air different components on/off for testing
* can be remotely powered via USB connector, or via controller 3.3V header pin 
* three M2.5 mounting holes
* will run 1.5" to 4.2" (maybe larger?) ePaper displays
* three epaper based Testpoints around the edges for easy access
  

## Available Software 
---------------
  
#### Arduino:
* `Code` - PlatformIO Arduino based code to test a ePaper display using the ePaper Name Badge board
